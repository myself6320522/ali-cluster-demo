data "alicloud_vpcs" "existing_vpc" {
  ids = [var.vpc_id]
}

resource "alicloud_vswitch" "ken_test_vsw" {
  vswitch_name = "ken_test_vsw"
  vpc_id       = data.alicloud_vpcs.existing_vpc.vpcs[0].id
  cidr_block   = "10.101.50.0/24"
  zone_id      = "cn-hongkong-b"
}

# 修改 security group 的定义，使用 data 源查询到的 VPC ID
resource "alicloud_security_group" "Ken_sg" {
  name   = "ken_aliyun_sg"
  vpc_id = data.alicloud_vpcs.existing_vpc.vpcs[0].id
}

resource "alicloud_security_group_rule" "Ken_sg_rule" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "22/22"
  priority          = 1
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_icmp" {
  type              = "ingress"
  ip_protocol       = "icmp"
  nic_type          = "intranet"
  policy            = "accept"
  priority          = 2  
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_https" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "443/443"
  priority          = 3
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_http" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "80/80"
  priority          = 4
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_10250" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "10250/10250"
  priority          = 5  # 注意优先级应根据您的具体需要进行调整
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_30000_32999" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "20000/32999"
  priority          = 6
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_Grafana" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "3000/3000"
  priority          = 3
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_intranet" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  policy            = "accept"
  port_range        = "1/65535"
  priority          = 6  # 注意优先级应根据您的具体需要进行调整
  security_group_id = alicloud_security_group.Ken_sg.id
  cidr_ip           = "10.0.0.0/8"
}

resource "tls_private_key" "rsa-4096-test" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "alicloud_ecs_key_pair" "publickey" {
  key_pair_name = var.key_name
  public_key    = tls_private_key.rsa-4096-test.public_key_openssh
}

resource "local_file" "key" {
  filename = var.key_name
  # 文件内容
  content  = tls_private_key.rsa-4096-test.private_key_pem
}

resource "alicloud_ecs_key_pair_attachment" "publickey" {
  key_pair_name = alicloud_ecs_key_pair.publickey.key_pair_name
  instance_ids  = alicloud_instance.Ken_instance.*.id
}

resource "alicloud_instance" "Ken_instance" {
  count                      = var.instance_count
  availability_zone          = "cn-hongkong-b"
  security_groups            = [alicloud_security_group.Ken_sg.id]
  instance_type              = "ecs.n1.medium"
  system_disk_category       = "cloud_ssd"
  image_id                   = "ubuntu_22_04_x64_20G_alibase_20230907.vhd"
  instance_name              = "ken-cluster"
  vswitch_id                 = alicloud_vswitch.ken_test_vsw.id
  internet_max_bandwidth_out = 1
  user_data       = <<EOF
#!/bin/sh

sudo apt-get update
sudo apt-get install ca-certificates curl -y
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install containerd.io
sudo systemctl enable containerd

sudo mkdir -p /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml

if [ ! -f /etc/containerd/config.toml ]; then
    echo "File /etc/containerd/config.toml does not exist."
    exit 1
fi

sudo sed -i '127 s/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
sudo echo "SystemdCgroup has been set to true in /etc/containerd/config.toml"
sudo systemctl restart containerd

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# 如果顯示錯誤"etc/apt/keyrings does not exist"。請先執行"sudo mkdir -p -m 755 /etc/apt/keyrings，再重新curl

echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt-cache madison kubeadm

sudo apt-get update
sudo apt-get install -y kubelet=1.29.1-1.1 kubeadm=1.29.1-1.1 kubectl=1.29.4-2.1
sudo apt-mark hold kubelet kubeadm kubectl

sudo modprobe overlay
sudo modprobe br_netfilter
sudo echo -e  'overlay\nbr_netfilter' | sudo tee /etc/modules-load.d/containerd.conf
sudo tee /etc/sysctl.d/kubernetes.conf <<INNER_EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
INNER_EOF

sudo sysctl --system
sudo apt install bash-completion
echo 'source <(kubectl completion bash)' >>~/.bashrc
source ~/.bashrc

EOF
}


# ---------------------------------------------------------------------------------------
