terraform {
  required_providers {
    alicloud = {
      source  = "aliyun/alicloud"
    }
  }
  required_version = ">= 1.8.1"
}

provider "alicloud" {
  region     = "cn-hongkong"
#   version = "~> 1.223.1"
#   access_key = "***********"
#   secret_key = "***********"
}
