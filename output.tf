output "public_key" {
  value = tls_private_key.rsa-4096-test.public_key_pem
  sensitive = true
}

# 生成的key
output "private_key" {
  value = tls_private_key.rsa-4096-test.private_key_pem
  sensitive = true
}

output "instance_ips" {
  value = alicloud_instance.Ken_instance.*.public_ip
}

output "private_ip" {
  value = alicloud_instance.Ken_instance.*.private_ip
}