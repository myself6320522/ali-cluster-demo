variable "key_name" {
  default = "tls_key"
  description = "diliver_key"
}

variable "instance_count" {
  default = 2
  description = "Define the number of instance creation."
  type        = number
  }

  variable "vpc_id" {
  default = "vpc-j6cj17xuub3w3wn5x8tbu"
}

#   variable "nat_id" {
#   type = string
#   default = "ngw-j6co9fwr7ntr11b0wr6e6"
# }

#   variable "snat_table_id" {
#   default = "vtb-j6cgi8crsrehl09pd9pq4"
# }